/*
    Copyright (C) 2011, Doti, deviatenow.com
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package ss;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
/**
* 
* @author Doti
* 
*/
public class PropertiesWorker {

	private static final String BASE_TEXT_IN_FILE = "This is a text file, encoding ISO-8859-1";
	
	private MainWindow main;
	private String filename;

	public PropertiesWorker(MainWindow main, String filename) {
		this.main = main;
		this.filename = filename;
	}
	
	public void updateSystemProperties() {
		Properties prop = new Properties();
		try {
			FileInputStream fis = new FileInputStream(new File(
					main.getDataFolder(), filename));
			prop.load(fis);
			for(Object okey:prop.keySet()) {
				String key = (String) okey;
				if(key.startsWith("properties."))
					System.setProperty(key.substring("properties.".length()), 
							(String)prop.get(key));
			}
				
		} catch (FileNotFoundException e) {
			try {
				FileWriter fos = new FileWriter(new File(
						main.getDataFolder(), filename));
				fos.write(BASE_TEXT_IN_FILE+"\n");
				fos.close();
			} catch(IOException ex) {
				main.showMessage(main.getString("error", e)+"\nCheck folder rights", true);
			}
			return ;
		} catch (IOException e) {
			main.showMessage(main.getString("error", e), true);
			return ;
		}
	}

	public synchronized String loadString(String key) {
		Properties prop = new Properties();
		try {
			FileInputStream fis = new FileInputStream(new File(
					main.getDataFolder(), filename));
			prop.load(fis);
			fis.close();				
			return prop.getProperty(key);
		} catch (FileNotFoundException e) {
			main.showMessage(main.getString("error", e), true);
			return null;
		} catch (IOException e) {
			main.showMessage(main.getString("error", e), true);
			return null;
		}
	}

	public Integer loadInteger(String key) {
		String v = loadString(key);
		if (v == null)
			return null;
		Double d = new Double(v);
		return d.intValue();
	}

	public Float loadFloat(String key) {
		String v = loadString(key);
		if (v == null)
			return null;
		return new Float(v);
	}

	public Boolean loadBoolean(String key) {
		String v = loadString(key);
		if (v == null)
			return null;
		return new Boolean(v);
	}

	@SuppressWarnings("unchecked")
	public Map<String,?> loadMap(String key) {
		Object o = load(key);
		if(o instanceof Map<?,?>)
			return (Map<String,?>) o;
		else
			return null;
	}
	
	public Object load(String key) {
		Properties prop = new Properties();
		try {
			FileInputStream fis = new FileInputStream(new File(
					main.getDataFolder(), filename));
			prop.load(fis);
			fis.close();		
			
			Object o = getObjectFromPropertiesListRec(key, prop);
			return o;
		} catch (FileNotFoundException e) {
			main.showMessage(main.getString("error", e), true);
			return null;
		} catch (IOException e) {
			main.showMessage(main.getString("error", e), true);
			return null;
		}
	}

	public String loadFirstTrue(String... keys) {
		for (String key : keys) {
			String v = loadString(key);
			if (v != null && new Boolean(v) == true)
				return key;
		}
		return null;
	}
	
	public synchronized void save(String key, Object v) {
		Properties prop = new Properties() {
			private static final long serialVersionUID = 1L;

			@Override
			public synchronized Enumeration<Object> keys() {
				List<Object> l = Collections.list(super.keys());
				Collections.sort(l, new Comparator<Object>() {
					@Override
					public int compare(Object o1, Object o2) {
						return ((String) o1).compareTo((String) o2);
					}
				});
				return Collections.enumeration(l);
			}
		};
		try {
			try {
				FileInputStream fis = new FileInputStream(new File(
						main.getDataFolder(), filename));
				prop.load(fis);
				fis.close();
			} catch (FileNotFoundException e) {
			}
			
			//remove all previous values in any case
			prop.remove(key);
			Enumeration<?> e = prop.propertyNames();
			while(e.hasMoreElements()) {
				String k = (String)e.nextElement(); 
				if(k.startsWith(key) && k.length()>key.length() && k.charAt(key.length())=='.') {
					prop.remove(k);
				}
			}
			
			//then maybe save something
			if(v!=null) {
				Map<String,String> lines = getPropertiesListFromObjectRec(key, v);
				for(String detailKey:lines.keySet()) {
					prop.put(detailKey, lines.get(detailKey));							
				} 
			}
			
			FileOutputStream fos = new FileOutputStream(new File(
					main.getDataFolder(), filename));
			prop.store(fos, BASE_TEXT_IN_FILE);
			fos.close();
		} catch (IOException e) {
			main.showMessage(main.getString("error", e), true);
		}
	}

	public void backup() throws IOException  {
		copyFile(filename, filename+".bak");
	}

	public void restore() throws IOException  {
		copyFile(filename+".bak", filename);
		new File(filename+".bak").deleteOnExit();
	}

	private void copyFile(String inputFile, String outputFile)
			throws IOException {

		FileReader in = new FileReader(inputFile);
		FileWriter out = new FileWriter(outputFile);
		int c;

		while ((c = in.read()) != -1)
			out.write(c);

		in.close();
		out.close();
	}
	

	@SuppressWarnings("unchecked")
	public static Map<String,String> getPropertiesListFromObjectRec(String baseKey, Object v) {
		Map<String,String> res = new HashMap<String, String>();
		if(v instanceof Map<?,?>) {
			for(String subkey:((Map<String, ?>)v).keySet()) {
				res.putAll(getPropertiesListFromObjectRec(baseKey+"."+subkey, ((Map<String, ?>)v).get(subkey)));
			}
		} else if(v instanceof Iterable<?>) {
			int n = 0;
			for(Object subv:((Iterable<?>)v)) {
				res.putAll(getPropertiesListFromObjectRec(baseKey+"."+n, subv));
				n++;
			}
		} else {
			res.put(baseKey,  String.valueOf(v));			
		}
		return res;
	}

	/**
	 * Needed by loadMap
	 * @param baseKey base key, example "test.fullmap" 
	 * @param prop full properties
	 * @return a value, list or map
	 */
	public static Object getObjectFromPropertiesListRec(String baseKey, Properties prop) {
		if(prop.containsKey(baseKey)) {
			String v = prop.getProperty(baseKey);
			try {
				return Integer.parseInt(v);				
			} catch(Exception ex){}
			try {
				return Float.parseFloat(v);				
			} catch(Exception ex){}			
			// "Bill" parsed as a boolean returns false, not an exception 
			//return Boolean.parseBoolean(v);
			if(v.equalsIgnoreCase("true"))
				return true;
			if(v.equalsIgnoreCase("false"))
				return false;
			if(v.equalsIgnoreCase("null"))
				return null;
			return v;
		}

		boolean isArray = false, isMap = false;
		Enumeration<?> e = prop.propertyNames();
		while(e.hasMoreElements()) {
			String k = (String)e.nextElement(); 
			if(k.startsWith(baseKey) && k.length()>baseKey.length() && k.charAt(baseKey.length())=='.') {
				String kSubstr = k.substring(baseKey.length()+1);
				if(kSubstr.matches("^\\d{1,3}.*") && !isMap) {				
					isArray = true;
					break;
				} else {
					isMap = true;
					break;
				}				
			}
		}		
		
		if(isArray) {
			List<Object> l = new ArrayList<Object>();
			Enumeration<?> e2 = prop.propertyNames();
			while(e2.hasMoreElements()) {
				String k = (String)e2.nextElement();				
				if(k.startsWith(baseKey) && k.length()>baseKey.length() && k.charAt(baseKey.length())=='.') {
					String baseSubkey = k;
					if(baseSubkey.substring(baseKey.length()+1).contains("."))
						baseSubkey = baseSubkey.substring(0, baseSubkey.indexOf(".", baseKey.length()+1));
					String midKey = baseSubkey.substring(baseKey.length()+1);
					int num = Integer.parseInt(midKey);
					while(num>=l.size())
						l.add(null);
					l.set(num, getObjectFromPropertiesListRec(baseSubkey, prop));
				}
			}
			return l;
		} else if(isMap) {
			Map<String,Object> m = new HashMap<String, Object>();
			Enumeration<?> e2 = prop.propertyNames();
			while(e2.hasMoreElements()) {
				String k = (String)e2.nextElement();				
				if(k.startsWith(baseKey) && k.length()>baseKey.length() && k.charAt(baseKey.length())=='.') {
					String baseSubkey = k;
					if(baseSubkey.substring(baseKey.length()+1).contains("."))
						baseSubkey = baseSubkey.substring(0, baseSubkey.indexOf(".", baseKey.length()+1));
					String midKey = baseSubkey.substring(baseKey.length()+1);
					m.put(midKey, getObjectFromPropertiesListRec(baseSubkey, prop));
				}
			}
			// key A.B.C.D, baseKey = A.B :
			// baseSubkey = A.B.C
			// midKey = C
			return m;			
		} else {
			return null;
		}
	}
}
