/*
    Copyright (C) 2011, Doti, deviatenow.com
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package ss.android;

import android.app.ListActivity;
import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListAdapter;

import java.security.InvalidParameterException;

/**
 * Used from scripts, show multiple options window
 *
 * @author Doti
 */
public class OptionsActivity extends ListActivity {

	private String[] values;
	private boolean[] checked;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle(getIntent().getExtras().getString("title"));
		values = getIntent().getExtras().getStringArray("values");
		if(savedInstanceState!=null && savedInstanceState.containsKey("checked"))
			checked = savedInstanceState.getBooleanArray("checked");
		else {
//		buggy buggy buggy checked = getIntent().getExtras().getBooleanArray("defaultValues");
			String[] checkedTmp = getIntent().getExtras().getStringArray("defaultValues");
			checked = new boolean[checkedTmp.length];
			for (int i = 0; i < checked.length; i++)
				checked[i] = Boolean.parseBoolean(checkedTmp[i]);
			if (values == null || checked == null || values.length != checked.length)
				throw new InvalidParameterException("getBooleans() : invalid parameters (" +
						getIntent().getExtras().toString() + ")");
		}
		//in case of no click anywhere
		Intent i = new Intent();
		i.putExtra("result", checked);
		setResult(MainActivity.RESULT_OPTIONS, i);
		setListAdapter(new OptionsAdapter());
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putBooleanArray("checked",checked);
	}

	private class OptionsAdapter  implements ListAdapter, OnClickListener {

		@Override
		public int getCount() {
			return values.length;
		}

		@Override
		public Object getItem(int position) {
			return values[position];
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public int getItemViewType(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LinearLayout ll = new LinearLayout(OptionsActivity.this);
			ll.setOrientation(LinearLayout.HORIZONTAL);
			CheckBox cb = new CheckBox(OptionsActivity.this);
			ll.addView(cb);
			cb.setChecked(checked[position]);
			cb.setText(values[position]);
			cb.setOnClickListener(this);
			cb.setId(1000+position);
			return ll;
		}

		@Override
		public int getViewTypeCount() {
			return 1;
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

		@Override
		public boolean isEmpty() {
			return values.length>0;
		}

		@Override
		public void registerDataSetObserver(DataSetObserver observer) {
		}

		@Override
		public void unregisterDataSetObserver(DataSetObserver observer) {
		}

		@Override
		public boolean areAllItemsEnabled() {
			return true;
		}

		@Override
		public boolean isEnabled(int position) {
			return true;
		}

		@Override
		public void onClick(View view) {
		    int id = view.getId();
		    id -= 1000;
			checked[id] = !checked[id];
			Intent i = new Intent();
			i.putExtra("result", checked);
			setResult(MainActivity.RESULT_OPTIONS, i);
		}		
	}

}
