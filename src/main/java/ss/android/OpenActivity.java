/*
    Copyright (C) 2012, Doti, deviatenow.com
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package ss.android;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

/**
 * 
 * Returns : chosen scriptName
 * 
 * @author Doti
 * 
 */
public class OpenActivity extends ListActivity {

	private List<Map<String, String>> list;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		list = new ArrayList<Map<String, String>>();
		try {
			String[] scriptList = getIntent().getStringArrayExtra("scriptList");
			for(String fileName : scriptList) {
				Map<String, String> m = new HashMap<String, String>();
				m.put("fileName", fileName);
				String name = fileName;
				if(name.endsWith(".groovy.apk"))
					name = name.substring(0, name.indexOf(".groovy.apk"));
				m.put("name", name);
				list.add(m);
			}
			Log.d("OpenActivity", "onCreate() : " + list.size() + " items");
		} catch (Exception e) {
			Log.e("OpenActivity", "Error : " + e);
		}
		setListAdapter(new ColorfullSimpleAdapter(this, list, R.layout.open_list_item,
				new String[] { "name"}, new int[] { R.id.open_name }));
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		Intent intent = new Intent();
		intent.putExtra("name", list.get(position).get("name"));
		setResult(MainActivity.RESULT_OPEN, intent);
		finish();
	}
}
