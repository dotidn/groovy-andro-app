/*
    Copyright (C) 2011, Doti, deviatenow.com
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package ss.android;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import ss.TextSource;

/**
 * Multiple use Async Task
 *
 * @author Doti
 */
public abstract class ProgressAsyncTask<T1,T2> extends AsyncTask<T1, Integer, T2> {

	protected ProgressDialog pd; 
	protected Context context;
	protected TextSource textSource;
	
	public ProgressAsyncTask(Context context, TextSource textSource) {
		super();
		this.context = context;
		this.textSource = textSource;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		pd = ProgressDialog.show(context, textSource.getString("title"),
				textSource.getString("android.pleasewait"), true);
	}

	@Override
	protected void onPostExecute(T2 result) {
		pd.cancel();
		super.onPostExecute(result);
	}

}
